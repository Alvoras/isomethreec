function init() {
    //resetLS();
    console.log(localStorage);
    handleLocalSession();
    // Create camera object
    initIsometricCamera();
    // initPerspectiveCamera(70);
    //
    var listener = new THREE.AudioListener({
        autoStart : true,
    });

    camera.add(listener);


    initWarpSound = new THREE.Audio(listener);
    warpEngagedSound = new THREE.Audio(listener);

    // create a global audio source
    let soundscape = new THREE.Audio(listener);

    // load a sound and set it as the Audio object's buffer
    let audioLoader = new THREE.AudioLoader();
    audioLoader.load('sound/soundscape.wav', function(buffer) {
        soundscape.setBuffer(buffer);
        soundscape.setLoop(true);
        soundscape.setVolume(0.5);
        soundscape.play();
    });

    // load a sound and set it as the Audio object's buffer
    audioLoader.load('sound/gate1.mp3', function(buffer) {
        initWarpSound.setBuffer(buffer);
        initWarpSound.setLoop(false);
        initWarpSound.setVolume(0.7);
        initWarpSound.pause();
    });

    audioLoader.load('sound/warpdrive_active.mp3', function(buffer) {
        warpEngagedSound.setBuffer(buffer);
        warpEngagedSound.setLoop(false);
        warpEngagedSound.setVolume(0.5);
        warpEngagedSound.pause();
    });

    initGUI();

    initProbsAttributes(planets)

    currentState = GAME_STATE.MAIN;

    // Create scene
    // Global
    scene = new THREE.Scene();

    let axes = new THREE.AxesHelper(1000);
    axes.position.set(0, 0, 0);
    scene.add(axes);

    // Items creation
    /*let skybox = new THREE.SphereGeometry(4000, 32, 32);
    let material = new THREE.MeshBasicMaterial({
        map: texture.skybox,
        side: THREE.BackSide
    });
    let sphere = new THREE.Mesh(skybox, material);
    scene.add(sphere);*/

    //draw2DGround(groundMap2D);
    //draw3DGround(groundMap3D);

    blocker = document.getElementById("blocker");

    generatePlanet();

    createChar();
    interfaceMinMap();

    // Light
    let light = new THREE.DirectionalLight(0x8888888);
    //light2.position.set(-1,-1,0);
    scene.add(light);

    let ambientLight = new THREE.AmbientLight(0x777777); // soft white light
    scene.add(ambientLight);

    let coreLight = new THREE.PointLight(0x6495ed);
    coreLight.position.set(0, 0, 0);
    scene.add(coreLight);

    /*
        let light2 = new THREE.PointLight( 0xff7f24, 6, 1000 );
        light2.position.set(250, 0, 750 );
        light2.castShadow = true;            // default false
        light2.shadow.mapSize.width = 1024;  // default 512
        light2.shadow.mapSize.height = 1024; // default 512
        light2.shadow.camera.near = 2;       // default 0.5
        light2.shadow.camera.far = 1500;
        	scene.add(light2);
    */
    /*
           let light3 = new THREE.PointLight( 0x6495ed, 6, 1000 );
        light3.position.set(-250, -100, 0 );
        light3.castShadow = true;            // default false
        light3.shadow.mapSize.width = 1024;  // default 512
        light3.shadow.mapSize.height = 1024; // default 512
        light3.shadow.camera.near = 2;       // default 0.5
        light3.shadow.camera.far = 1500;
        	scene.add(light3);
    */

    // Create renderer object
    // Used to render/diplay the scene
    // Global
    renderer = new THREE.WebGLRenderer();

    // Tune the pixel ratio depending on the device
    renderer.setPixelRatio(window.devicePixelRatio);

    // Set the size of the renderer
    // Here it takes the whole parent dom element
    renderer.setSize(w, h);

    //controls = new THREE.OrbitControls(camera, renderer.domElement);
    //controls.addEventListener('change', renderer);
    //controls.enableZoom = true;
    //controls.enabled = false;

    // Make the camera a child of the scene so we add items to it
    scene.add(camera);


    /*loader.load("http://localhost/isomethreec/obj/the_grid.obj", (object) => {
        console.log(object);
        object.scale.set(20,20,20);
        object.position.x = 0;
        object.position.y = -100;
        object.position.z = -100;
        object.rotation.x = Math.PI / 2;

        object.side = THREE.DoubleSide;

        camera.add(object);
    }, (xhr) => {
        console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
    }, (err) => {
        console.log( 'An error happened', err );
    });
*/

    raycaster = new THREE.Raycaster();

    document.addEventListener("keydown", function(e) {
        switch (e.keyCode) {
            case keymap.escape:
                blocker.style.display = '';
                if (minMap != 0) {
                    zoomParameter = 1;
                    printMinMap = 1;
                    minMap = 1;
                } else {
                    zoomParameter = 2;
                    printMinMap = 0;
                    minMap = 0;
                }
                break;
        }
        if (currentState == GAME_STATE.MAIN) {
            switch (e.keyCode) {
                case keymap.Z:
                    planets.children[activePlanetIndex].moveState[MOVE.FRONT] = 1;
                    char.moveState[MOVE.BACK] = 1;
                    char.facing = FACING.BACK;
                    break;
                case keymap.S:
                    planets.children[activePlanetIndex].moveState[MOVE.BACK] = 1;
                    char.moveState[MOVE.FRONT] = 1;
                    char.facing = FACING.FRONT;
                    break;
                case keymap.Q:
                    planets.children[activePlanetIndex].moveState[MOVE.RIGHT] = 1;
                    char.moveState[MOVE.LEFT] = 1;
                    char.facing = FACING.LEFT;
                    break;
                case keymap.D:
                    planets.children[activePlanetIndex].moveState[MOVE.LEFT] = 1;
                    char.moveState[MOVE.RIGHT] = 1;
                    char.facing = FACING.RIGHT;
                    break;
                case keymap.jump:
                    char.isJumping = true;
                    char.isFalling = false;
                    break;
                case keymap.keyUp:
                    shoot(FACING.BACK);
                    break;
                case keymap.keyDown:
                    shoot(FACING.FRONT);
                    break;
                case keymap.keyLeft:
                    shoot(FACING.LEFT);
                    break;
                case keymap.keyRight:
                    shoot(FACING.RIGHT);
                    break;

                case keymap.shoot:
                    //shoot();
                    break;
            }
        }

    });

    document.addEventListener("keyup", function(e) {
        switch (e.keyCode) {
            case keymap.Z:
                planets.children[activePlanetIndex].moveState[MOVE.FRONT] = 0;
                char.moveState[MOVE.BACK] = 0;
                char.rotation.set(0, 0, 0);
                break;
            case keymap.S:
                planets.children[activePlanetIndex].moveState[MOVE.BACK] = 0;
                char.moveState[MOVE.FRONT] = 0;
                char.rotation.set(0, 0, 0);
                break;
            case keymap.Q:
                planets.children[activePlanetIndex].moveState[MOVE.RIGHT] = 0;
                char.moveState[MOVE.LEFT] = 0;
                char.rotation.set(0, 0, 0);
                break;
            case keymap.D:
                planets.children[activePlanetIndex].moveState[MOVE.LEFT] = 0;
                char.moveState[MOVE.RIGHT] = 0;
                char.rotation.set(0, 0, 0);
                break;
            case keymap.jump:
                char.velY = -20;
                char.isJumping = true;
                break;
            case keymap.moveCamLeft:
                moveCamera(MOVE.LEFT);
                break;
            case keymap.moveCamRight:
                moveCamera(MOVE.RIGHT);
                break;
        }

        resetFrame();
    });


    // One chance out of 3
    //randomizeAttribute(planets.children[activePlanetIndex]);
    //randomizeCore(3);

    generatePointCloud();

    blocker.addEventListener('click', function(e) {
        blocker.style.display = 'none';
    }, false);

    document.addEventListener('mousedown', removeBloc, false);
    document.addEventListener('mousedown', choosePlanet, false);

    document.addEventListener('mousemove', (e) => {
        highlightBloc(e);
        //targetCore(e);
    }, false);

    document.addEventListener('mousedown', function(e) {
        if ((targetCore(e))) {
            addToCollection(currentCore.typeId);
        }
    }, false);

    // Insert the renderer to the dom
    document.body.appendChild(renderer.domElement);
    stats = new Stats();
    document.body.appendChild(stats.dom);

    //createTunnel();
}

function generatePlanet() {
    let attributes = randomizeAttribute();

    console.log("OBTAINED ATTRIBUTES", attributes);

    planets.add(createPlanet(100, 25, 35, 20.0, attributes));

    initProb(planets.children[0]);

    console.log(planets.children[0]);

    planets.add(createPlanet(50, 25, 35, 20.0, attributes));
    planets.children[1].visible = false;
    initProb(planets.children[1]);

    planets.add(createPlanet(50, 25, 35, 20.0, attributes));
    planets.children[2].visible = false;
    initProb(planets.children[2]);

    scene.add(planets);
}

function animate() {
    // Request frame refresh as soon as the computing is over
    requestAnimationFrame(animate);

    switch (currentState) {
        case GAME_STATE.MAIN:
            if ((doShakePlanet)) {
                shakePlanet();
            }

            animateChar();
            wallHitbox();
            animatePlanet();
            animateShell();
            animateShot();
            break;
        case GAME_STATE.INVENTORY:
            spinDisplayedCore();
            spinCoreHolder();
            break;

        case GAME_STATE.WARP:
            animateWarp();
            break;

        default:

    }

    updatePointCloud();

    if (minMap) {
        if (zoom(zoomParameter) != 2) {
            //camera.position.set(char.position.x + 500, char.position.y + 500, char.position.z + 500);
            //camera.lookAt(char.position.x, char.position.y, char.position.z);
        }
    } else {
        if (currentState == GAME_STATE.MAIN) {
            lockCamOnChar();
        }
    }

    frameCount++;
    stats.update();
    renderer.render(scene, camera);

    verticalHitbox();
    keepOut();
}

function animateShell() {
    if ((planets.children[activePlanetIndex].attributes.hasShell.status)) {
        shellGroup[0].rotation.x += Math.random() * 0.0005;
        //shellGroup[i].rotate.y = Math.random() * 0.0005;
        shellGroup[0].rotation.z -= Math.random() * 0.0005;

        shellGroup[1].rotation.x -= Math.random() * 0.0005;
        shellGroup[1].rotation.z += Math.random() * 0.001;

        shellGroup[2].rotation.x += Math.random() * 0.0001;
        shellGroup[2].rotation.z -= Math.random() * 0.0005;
    }
}

function zoom(param) {
    if (param == 1) {
        if (char.position.y < 5000) char.position.y += 20;
        else {
            displayMinMap();
            return 2;
        }
    } else if (param == 2) {
        if (char.position.y > 20) char.position.y -= 20;
        else minMap = 0;
    }
}

function takeoff(mode) {
    switch (mode) {
        case TAKEOFF:
            while (char.position.y < SPACE_HEIGHT) {
                if (char.speedup < char.maxSpeed) {
                    char.velTakeoff += char.speedup;
                }
                char.position.y += char.velTakeoff;
            }
            displayMinMap();
            return 2; // ?
            break;
        case LANDING:
            while (char.position.y > GROUND_LEVEL) { // Raycaster, why not
                if (char.speedup < char.maxSpeed) {
                    char.velLanding += char.speedup;
                }
                char.position.y -= char.velLanding;
            }
            minMap = 0;
            break;
    }
}

function createChar() {
    let size = 20;

    char = createCube(size, size, size, groundTexture[0]);
    char.size = 20;
    //char.position.set(0, GROUND_LEVEL + char.size*2, 0);
    char.position.set(0, 250, 0);
    char.moveState = [0, 0, 0, 0];
    char.isJumping = false;
    char.isFalling = true;
    planets.children[activePlanetIndex].gravity = 5;
    char.frictionY = 0.9;
    char.facing = FACING.FRONT;
    char.velY = 0;
    char.velX = 3;
    char.friction = 1.1;
    char.currentFrame = 0;
    char.playAnimation = 0;
    char.animationState = rotationPerFrame * char.currentFrame;

    scene.add(char);
}

function resetFrame() {
    char.currentFrame = 0;
    char.playAnimation = 0;
}

function keepOut() {
    if (char.position.y < planets.children[activePlanetIndex].position.y + planets.children[activePlanetIndex].radius) {
        // char has fallen insidde
        char.position.y = planets.children[activePlanetIndex].position.y + planets.children[activePlanetIndex].radius + char.size
    }
}

function animatePlanet() {
    rotationPerFrame = (Math.PI / 2) / animFramePerSec;

    if ((planets.children[activePlanetIndex].moveState[MOVE.BACK])) {
        //scene.rotation.x -= planets.children[activePlanetIndex].animationSpeed;
        planets.children[activePlanetIndex].rotation.x -= planets.children[activePlanetIndex].animationSpeed;
    }
    if ((planets.children[activePlanetIndex].moveState[MOVE.FRONT])) {
        //scene.rotation.x += planets.children[activePlanetIndex].animationSpeed;
        planets.children[activePlanetIndex].rotation.x += planets.children[activePlanetIndex].animationSpeed;
    }
    if ((planets.children[activePlanetIndex].moveState[MOVE.LEFT])) {
        //scene.rotation.z += planets.children[activePlanetIndex].animationSpeed;
        planets.children[activePlanetIndex].rotation.z += planets.children[activePlanetIndex].animationSpeed;
    }
    if ((planets.children[activePlanetIndex].moveState[MOVE.RIGHT])) {
        //scene.rotation.z -= planets.children[activePlanetIndex].animationSpeed;
        planets.children[activePlanetIndex].rotation.z -= planets.children[activePlanetIndex].animationSpeed;
    }

}

function animateChar() {
    if ((char.moveState[MOVE.BACK])) {
        //char.position.z -= char.velX;
        char.rotation.x = -(rotationPerFrame * char.currentFrame);
        char.playAnimation = 1;
        char.rotation.z = 0;
    }
    if ((char.moveState[MOVE.FRONT])) {
        //char.position.z += char.velX;
        char.rotation.x = rotationPerFrame * char.currentFrame;
        char.playAnimation = 1;
        char.rotation.z = 0;
    }
    if ((char.moveState[MOVE.LEFT])) {
        //char.position.x -= char.velX;
        char.rotation.z = rotationPerFrame * char.currentFrame;
        char.playAnimation = 1;
        char.rotation.x = 0;
    }
    if ((char.moveState[MOVE.RIGHT])) {
        //char.position.x += char.velX;
        char.rotation.z = -(rotationPerFrame * char.currentFrame);
        char.playAnimation = 1;
        char.rotation.x = 0;
    }

    if ((char.playAnimation)) {
        char.currentFrame++;
    }

    if ((char.isJumping)) {
        char.velY -= planets.children[activePlanetIndex].gravity;
        char.velY *= char.frictionY;
        char.position.y -= char.velY;
    }

    if ((!(char.isFalling))) {
        planets.children[activePlanetIndex].gravity *= 0.09;
        if (char.velY < 0) {
            char.isFalling = true;
            char.isJumping = false;
            char.velY = 20;
            planets.children[activePlanetIndex].gravity = 2;
            //char.velY = 0;
        }
    }

    if ((char.isFalling)) {
        char.velY -= planets.children[activePlanetIndex].gravity;
        //char.velY *= char.frictionY;
        char.position.y += char.velY;
    }

}

function animateShot() {
    let i = 0;
    /*
    // Relative to char position
      for (i = 0; i < shotList[FACING.BACK].length; i++) {
        shotList[FACING.BACK][i].position.z -= 10;

        if (shotList[FACING.BACK][i].position.z - char.position.z > -200) {
          scene.remove(shotList[FACING.BACK][i]);
        }
      }

      for (i = 0; i < shotList[FACING.FRONT].length; i++) {
        shotList[FACING.FRONT][i].position.z += 10;

        if (shotList[FACING.BACK][i].position.z - char.position.z > 200) {
          scene.remove(shotList[FACING.FRONT][i]);
        }
      }

      for (i = 0; i < shotList[FACING.LEFT].length; i++) {
        shotList[FACING.LEFT][i].position.x -= 10;

        if (shotList[FACING.LEFT][i].position.x - char.position.x > -200) {
          scene.remove(shotList[FACING.LEFT][i]);
        }
      }

      for (i = 0; i < shotList[FACING.RIGHT].length; i++) {
        shotList[FACING.RIGHT][i].position.x += 10;

        if (shotList[FACING.LEFT][i].position.x - char.position.x > 200) {
          scene.remove(shotList[FACING.RIGHT][i]);
        }
      }
    */

    // Absolute position
    for (i = 0; i < shotList[FACING.BACK].length; i++) {
        shotList[FACING.BACK][i].position.z -= 10;

        if (shotList[FACING.BACK][i].position.z > 200) {
            scene.remove(shotList[FACING.BACK][i]);
        }
    }

    for (i = 0; i < shotList[FACING.FRONT].length; i++) {
        shotList[FACING.FRONT][i].position.z += 10;

        if (shotList[FACING.FRONT][i].position.z > 200) {
            scene.remove(shotList[FACING.FRONT][i]);
        }
    }

    for (i = 0; i < shotList[FACING.LEFT].length; i++) {
        shotList[FACING.LEFT][i].position.x -= 10;

        if (shotList[FACING.LEFT][i].position.x > 200) {
            scene.remove(shotList[FACING.LEFT][i]);
        }
    }

    for (i = 0; i < shotList[FACING.RIGHT].length; i++) {
        shotList[FACING.RIGHT][i].position.x += 10;

        if (shotList[FACING.RIGHT][i].position.x > 200) {
            scene.remove(shotList[FACING.RIGHT][i]);
        }
    }
}

function shoot(direction) {
    let x = char.position.x;
    let y = char.position.y;
    let z = char.position.z;

    let shot;

    switch (direction) {
        case FACING.BACK:
            z -= 20;
            break;
        case FACING.FRONT:
            z += 20;
            break;
        case FACING.LEFT:
            x -= 20;
            break;
        case FACING.RIGHT:
            x += 20;
            break;
    }

    shot = createCube(5, 5, 5, groundTexture[0]);
    shot.position.x = x;
    shot.position.y = y;
    shot.position.z = z;

    shotList[direction].push(shot);

    scene.add(shot);
}

function initPerspectiveCamera(Fov) {
    camera = new THREE.PerspectiveCamera(Fov, aspectRatio);
    camera.position.z = 200;
}

function initIsometricCamera() {
    let d = 500;
    // OrthographicCamera( left, right, top, bottom, near, far )
    camera = new THREE.OrthographicCamera(-d * aspectRatio, d * aspectRatio, d, -d, 1, 20000);
    camera.position.set(500, 500, 500);
    camera.rotation.order = 'YXZ';
    //camera.rotation.y = - Math.PI / 4;
    camera.rotation.x = Math.atan(-1 / Math.sqrt(2));
    camera.rotation.y = cameraState[CAM_STATE.FRONT];
}

function onWindowResize() {
    camera.aspect = aspectRatio;
    camera.updateProjectionMatrix();

    renderer.setSize(aspectRatio);
}

function moveCamera(direction) {
    let i = 0;

    for (i = 0; i < cameraState.length; i++) {
        // Get current camera state
        if (camera.rotation.y === cameraState[i]) {
            break;
        }
    }

    switch (direction) {
        // Clockwise
        case MOVE.RIGHT:
            i++;
            if (i === 4) {
                i = 0;
            }
            break;
            // Counter clockwise
        case MOVE.LEFT:
            i--;
            if (i < 0) {
                i = 3;
            }
            break;
    }

    camera.rotation.y = cameraState[i];
}

function initGUI() {
    let charText = function() {
        this.w = 20;
        this.h = 20;
        this.depth = 20;
    }

    let specsText = function() {
        this.velY = 5;
        this.velX = 5;
        this.gravity = 5;
        this.animationSpeed = rotationPerFrame;
    }

    let text = new charText();

    let charFolder = gui.addFolder('Character');
    charFolder.add(text, 'w', 1, 50).onChange(function(val) {
        char.scale.x = val;
    });
    charFolder.add(text, 'h', 1, 50).onChange(function(val) {
        char.scale.y = val;
    });
    charFolder.add(text, 'depth', 1, 50).onChange(function(val) {
        char.scale.z = val;
    });

    text = new specsText();
    let specsFolder = gui.addFolder('Specifications');
    specsFolder.add(text, 'velX', 0, 1000).onChange(function(val) {
        gridHelper.width = val;
    });
    specsFolder.add(text, 'velY', 0, 20).onChange(function(val) {
        gridHelper.height = val;
    });
    specsFolder.add(text, 'gravity', 0, 30).onChange(function(val) {
        planets.children[activePlanetIndex].gravity = val;
    });
    specsFolder.add(text, 'animationSpeed', 0, 30).onChange(function(val) {
        animFramePerSec = val;
    });

    // char x, y, z
    // velY, velx, gravity,
}

function targetCore(e) {
    if (!(planets.children[activePlanetIndex].attributes.hasCore.status)) {
        return 0;
    }
    e.preventDefault();
    mouse.x = (e.clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(e.clientY / window.innerHeight) * 2 + 1;

    // find intersections
    raycaster.setFromCamera(mouse, camera);

    let arr = [];
    arr.push(currentCore);
  //  arr.push(planets.children[activePlanetIndex].children);
    let intersects = raycaster.intersectObjects(arr);

    if (intersects.length > 0) {
        if (intersects[0].object.material.emissive !== undefined) {
            intersects[0].object.material.emissive.setHex(0xbbbbbb);
        }
        return 1;
    }

    return 0;
}

function isCoreVisible() {
    if (!(planets.children[activePlanetIndex].attributes.hasCore.status)) {
        return 0;
    }
    raycaster.setFromCamera(currentCore.position, camera);

    let arr = [];
    arr.push(currentCore);

    let intersects = raycaster.intersectObjects(arr);

    console.log("is core visible intersects", intersects);
    if (intersects.length > 0) {
        console.log("is core visible intersects", intersects);
        if (intersects[0].object.material.emissive !== undefined) {
            intersects[0].object.material.emissive.setHex(0x99ff88);
            return 1;
        }
    }

    return 0;
}

function highlightBloc(e) {
    e.preventDefault();
    mouse.x = (e.clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(e.clientY / window.innerHeight) * 2 + 1;

    // find intersections
    raycaster.setFromCamera(mouse, camera);

    let tab = planets.children[activePlanetIndex].children;
    let intersects = raycaster.intersectObjects(tab);

    if (intersects.length > 0) {
        if (intersects[0].object.material.emissive !== undefined) {
            intersects[0].object.material.emissive.setHex(0xff0000);
        }
    }
}

function choosePlanet(event) {
    if (!(minMap)) {
        return;
    }
    event.preventDefault();
    mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;

    // find intersections
    raycaster.setFromCamera(mouse, camera);

    let intersects = raycaster.intersectObjects(circlesMinMap);

    if (intersects.length > 0) {
        if (intersects[0].object.material.emissive !== undefined) {
            intersects[0].object.material.emissive.setHex(0xff0000);
        }
    }
    activePlanetIndex = circlesMinMap.indexOf(intersects[0].object);
    planets.children[activePlanetIndex].visible = true;
    zoomParameter = 2;
}

function initProb(planet) {
    planet.attributes.hasCore.prob = 3;

    planet.attributes.hasAsteroids.prob = 3;

    planet.attributes.hasShell.prob = 10;

    planet.attributes.hasTail.prob = 10;

    planet.attributes.hasMovement.prob = 10;

    planet.attributes.hasStrongGravity.prob = 15;

    planet.attributes.hasWeakGravity.prob = 15;

    planet.attributes.isGiant.prob = 15;

    planet.attributes.isTiny.prob = 15;

    planet.attributes.nautilus.prob = 10;
}

function initProbsAttributes() {
    planets.attributes = {};

    planets.attributes.hasCore = {};
    planets.attributes.hasCore.status = 0;
    planets.attributes.hasCore.prob = 3;

    planets.attributes.hasAsteroids = {};
    planets.attributes.hasAsteroids.status = 0;
    planets.attributes.hasAsteroids.prob = 3;

    planets.attributes.hasShell = {};
    planets.attributes.hasShell.status = 0;
    planets.attributes.hasShell.prob = 10;

    planets.attributes.hasTail = {};
    planets.attributes.hasTail.status = 0;
    planets.attributes.hasTail.prob = 10;

    planets.attributes.hasMovement = {};
    planets.attributes.hasMovement.status = 0;
    planets.attributes.hasMovement.prob = 10;

    planets.attributes.hasStrongGravity = {};
    planets.attributes.hasStrongGravity.status = 0;
    planets.attributes.hasStrongGravity.prob = 15;

    planets.attributes.hasWeakGravity = {};
    planets.attributes.hasWeakGravity.status = 0;
    planets.attributes.hasWeakGravity.prob = 15;

    planets.attributes.isGiant = {};
    planets.attributes.isGiant.status = 0;
    planets.attributes.isGiant.prob = 15;

    planets.attributes.isTiny = {};
    planets.attributes.isTiny.status = 0;
    planets.attributes.isTiny.prob = 15;

    planets.attributes.nautilus = {};
    planets.attributes.nautilus.status = 0;
    planets.attributes.nautilus.prob = 10;
}

function verticalHitbox() {
    let i = 0;

    raycaster.set(char.position, VECTORS.DOWN);
    let intersects = raycaster.intersectObjects(planets.children[activePlanetIndex].children);

    if (intersects.length > 0) {
        //raycaster.set(char.position, VECTORS.DOWN);
        if (intersects[0].distance < char.size) {
            //if (char.position.y - intersects[0].object.position.y < char.size*2) {
            console.log(char.position.y - intersects[0].object.position.y, ' < ', char.size);
            char.position.y = intersects[0].object.position.y + (char.size * 1.5);
            char.velY = 1;
            char.isFalling = 0;
        }
    }
}

function wallHitbox(direction) {
    let vector = new THREE.Vector3();
    let pos;

    switch (direction) {
        case FACING.FRONT:
            vector = VECTORS.FRONT;
            break;
        case FACING.BACK:
            vector = VECTORS.BACK;
            break;
        case FACING.LEFT:
            vector = VECTORS.LEFT;
            break;

        case FACING.RIGHT:
            vector = VECTORS.RIGHT;
            break;
    }

    raycaster.set(char.position, vector);
    let intersects = raycaster.intersectObjects(planets.children[activePlanetIndex].children);

    if (intersects.length > 0) {
        if (intersects[0].distance <= char.size) {
            switch (direction) {
                case FACING.FRONT:
                    char.position.z = intersects[0].object.position.z - char.size;
                    break;
                case FACING.BACK:
                    char.position.z = intersects[0].object.position.z + char.size;
                    break;
                case FACING.LEFT:
                    char.position.x = intersects[0].object.position.x - char.size;
                    break;
                case FACING.RIGHT:
                    char.position.x = intersects[0].object.position.x + char.size;
                    break;
            }
        }
        intersects[0].object.material.emissive.setHex(0x00ff00);
    }
}

function removeBloc(event) {
    event.preventDefault();
    mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
    // find intersections

    raycaster.setFromCamera(mouse, camera);

    let tab = planets.children[activePlanetIndex].children;
    let intersects = raycaster.intersectObjects(tab);

    if (intersects.length > 0) {

        let intersect = intersects[0];
        scene.remove(intersect.object);
        tab.splice(tab.indexOf(intersect.object), 1);
    }
}
