// Const
const Fov = 70;
const Unit = 10;
const RightAngle = 33;

const DefaultSegments = 32;

var gui = new dat.GUI();


var groundMap2D = [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 1, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 1, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
];

GROUND_LEVEL = 0;

const CUBE_SIZE = 20;

const keymap = {
    keyUp: 38,
    keyDown: 40,
    keyLeft: 37,
    keyRight: 39,
    moveCamLeft: 74,
    moveCamRight: 77,
    jump: 32,
    shoot: 13,
    Z: 90,
    Q: 81,
    S: 83,
    D: 68,
    escape: 27,
    enter: 13
};

const MOVE = {
    BACK: 0,
    FRONT: 1,
    LEFT: 2,
    RIGHT: 3
};

const FACING = {
    BACK: 0,
    FRONT: 1,
    LEFT: 2,
    RIGHT: 3
};

const CAM_STATE = {
    FRONT: 0,
    BACK: 1,
    LEFT: 2,
    RIGHT: 3
};

const VECTORS = {
    UP: new THREE.Vector3(0, -1, 0),
    DOWN: new THREE.Vector3(0, 1, 0),
    LEFT: new THREE.Vector3(-1, 0, 0),
    RIGHT: new THREE.Vector3(1, 0, 0),
    FRONT: new THREE.Vector3(0, 0, -1),
    BACK: new THREE.Vector3(0, 0, 1),
}

const CORE_TYPE = {
    PLASTUER: 0,
    MARBLE: 1,
    STONE: 2,
    ROUGH: 3,
    ASPHALT: 4,
    ORANGE: 9,
    ABST_BUBBLY: 5,
    ABST_SQUARE: 6,
    METAL_PLATE: 7,
    METAL_PLATE_REGULAR: 8,
    STONE_FLOOR: 10,
    CORE_TOTAL: 11,
};

const ATTR = {
    CORE: 0,
    ASTEROIDS: 1,
    SHELL: 2,
    TAIL: 3,
    MOVEMENT: 4,
    GRAVITY_STRONG: 5,
    GRAVITY_WEAK: 6,
    GIANT: 7,
    TINY: 8,
    NAUTILUS: 9,
    ATTR_TOTAL: 10
};

const GAME_STATE = {
    MAIN: 0,
    MINI_MAP : 1,
    INVENTORY: 2,
    WARP: 3
};

let animFramePerSec = 15;

let rotationPerFrame = (Math.PI / 2) / animFramePerSec;

let shotList = [];
shotList[FACING.BACK] = [];
shotList[FACING.FRONT] = [];
shotList[FACING.LEFT] = [];
shotList[FACING.RIGHT] = [];

let currentState;

let planets = new THREE.Group();

let raycaster;

let removableItems = [];

let char;

let doShakePlanet = 0;

// Texture list
let texture = {};

texture.plastuer = {};
texture.plastuer.image = new THREE.TextureLoader().load('texture/core/plastuer/Plaster07_COL_VAR1_1K.jpg');
texture.plastuer.normalMap = new THREE.TextureLoader().load('texture/core/plastuer/Plaster07_NRM_1K.jpg');

texture.marble = {};
texture.marble.image = new THREE.TextureLoader().load('texture/core/marble/Marble13_COL_1K.jpg');
texture.marble.normalMap = new THREE.TextureLoader().load('texture/core/marble/Marble13_NRM_1K.jpg');

texture.stone = {};
texture.stone.image = new THREE.TextureLoader().load('texture/core/stone/WallMedieval003_COL_VAR1_1K.jpg');
texture.stone.normalMap = new THREE.TextureLoader().load('texture/core/stone/WallMedieval003_NRM_1K.jpg');

texture.rough = {};
texture.rough.image = new THREE.TextureLoader().load('texture/core/rough/CliffJagged004_COL_VAR1_1K.jpg');
texture.rough.normalMap = new THREE.TextureLoader().load('texture/core/rough/CliffJagged004_NRM_1K.jpg');

texture.asphalt = {};
texture.asphalt.image = new THREE.TextureLoader().load('texture/core/asphalt/Asphalt_001_COLOR.jpg');
texture.asphalt.normalMap = new THREE.TextureLoader().load('texture/core/asphalt/Asphalt_001_NRM.jpg');

texture.orange = {};
texture.orange.image = new THREE.TextureLoader().load('texture/core/orange/Orange_001_COLOR.jpg');
texture.orange.normalMap = new THREE.TextureLoader().load('texture/core/orange/Orange_001_NORM.jpg');

texture.abstractBubbly = {};
texture.abstractBubbly.image = new THREE.TextureLoader().load('texture/core/abstract_bubbly/Abstract_007_COLOR.jpg');
texture.abstractBubbly.normalMap = new THREE.TextureLoader().load('texture/core/abstract_bubbly/Abstract_007_NORM.jpg');

texture.abstractSquare = {};
texture.abstractSquare.image = new THREE.TextureLoader().load('texture/core/abstract_square/Abstract_006_COLOR.jpg');
texture.abstractSquare.normalMap = new THREE.TextureLoader().load('texture/core/abstract_square/Abstract_006_NORM.jpg');

texture.metalPlate = {};
texture.metalPlate.image = new THREE.TextureLoader().load('texture/core/metal_plate/Metal_Plate_002_COLOR.jpg');
texture.metalPlate.normalMap = new THREE.TextureLoader().load('texture/core/metal_plate/Metal_Plate_002_NORM.jpg');

texture.metalPlateRegular = {};
texture.metalPlateRegular.image = new THREE.TextureLoader().load('texture/core/metal_plate_regular/Metal_Plate_003_COLOR.jpg');
texture.metalPlateRegular.normalMap = new THREE.TextureLoader().load('texture/core/metal_plate_regular/Metal_Plate_003_NORM.jpg');

texture.stoneFloor = {};
texture.stoneFloor.image = new THREE.TextureLoader().load('texture/core/stone_floor/Stone_Floor_003_COLOR.jpg');
texture.stoneFloor.normalMap = new THREE.TextureLoader().load('texture/core/stone_floor/Stone_Floor_003_NORM.jpg');

//texture.brick = new THREE.TextureLoader().load('texture/brick_diffuse.jpg');
texture.skybox = new THREE.TextureLoader().load('texture/skybox.jpg');

let groundTexture = [];
groundTexture[0] = new THREE.TextureLoader().load('texture/brick_diffuse.jpg');
groundTexture[1] = new THREE.TextureLoader().load('texture/skybox.jpg');

// Point of view
let camera;

var loader = new THREE.OBJLoader();

let cameraState = [];
cameraState[0] = 0 + (-Math.PI / 4); // 0deg
cameraState[1] = Math.PI + (-Math.PI / 4); // 90deg
cameraState[2] = Math.PI / 2 + (-Math.PI / 4); // 180deg
cameraState[3] = -Math.PI / 2 + (-Math.PI / 4); // 270deg

let mouse = new THREE.Vector2(), INTERSECTED;

let controls;

let frameCount = 0;

let minMap = 0;
let printMinMap = 0;
let zoomParameter = 1;
let circlesMinMap = [];
let activePlanetIndex = 0;

let gridHelper;

// Environnement
let scene;

// Display
let renderer;

let spinCoreInHolder = false;
let spinHolder = false;
let currentDisplayedCore = 0;
let inventoryCore = [];
let displayGroup = [];
let inventoryGroup = [];

// Object
let mesh;

let w = window.innerWidth;
let h = window.innerHeight;
let aspectRatio = w/h;

let pointCloud;

let asteroids;

let currentCore;

let collection = [];
let hasSeen = [];

let shellGroup = [];

let blocker;

let clock = new THREE.Clock(false);

let previousShakeValuex = 0;
let previousShakeValuey = 0;
let previousShakeValuez = 0;
let previousShakeCase = 0;

let initWarpSound;
let warpEngagedSound;

let shakeCnt = 0;

let tunnelSwapped = 0;
let cruisingWarpTexture;
let warpTunnel;
let textureOffset = 0.0001;
let fovModifier = 2.5;
let fovFriction = 0.05;
let fovModified = 0;

let tunnelLight = [];

let isLightActive = false;

let varianceCounter = 0;
let varianceModifier = 0.05;
const WARP = {
    FASTER: 0,
    SLOWER: 1,
    CRUISING: 2,
    ARRIVAL: 3,
    INIT: 4,
    STANDBY: 5
};

let warpDuration = -1;
let warpStartTime = 0;
let warpPace = -1;
let currentWarpTexture;

// Init static elements
init();

// Start the animation loop
animate();
