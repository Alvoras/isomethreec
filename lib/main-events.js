$(document).ready(() => {
    $("body").click((e) => {
        console.log(e.target);
    })
    $("#go-button").click( () => {
        $("#welcome-card-container").addClass("fade-out");
        toggleFade();
        setTimeout( () => {
          checkDiscovered();
        }, 1000);
    });
    $("#inventory-button").click((e) => {
        toggleInventory();
        toggleTryptic();
    });

    $("#left-panel, #right-panel").click(function(e) {
        let direction = 1;
        if ($(this).attr("id") == "left-panel") {
            direction = -1;
        }

        swapCore(direction);
    });

    $("body").keyup(function(e) {
        switch (e.keyCode) {
            case keymap.escape:
                if (currentState != GAME_STATE.INVENTORY) {
                    toggleFade();
                    toggleTryptic();
                    restoreScene();
                    initIsometricCamera();
                    camera.position.set(char.position.x + 500, char.position.y + 500, char.position.z + 500);
                    camera.lookAt(char.position.x, char.position.y, char.position.z);
                    toggleFade();
                }
                break;
            case keymap.enter:
                if (currentState == GAME_STATE.MAIN) {
                    currentState = GAME_STATE.WARP;
                    initWarpCamera();
                    initWarp();
                }
            break;
            default:

        }
    });

    /*$("#blackscreen").click( (e) => {
        toggleFade();
        hideScene();
    });*/

});
