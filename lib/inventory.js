function toggleInventory() {
    toggleFade();

    cleanScene();

    unlockCam();
    initPerspectiveCamera(70);

    buildInventoryScene();

    currentState = GAME_STATE.INVENTORY;

    toggleFade();
}

function buildInventoryScene() {
    generatePointCloud();
    addCoreHolder();
    addLighting();
    addCoreToScene();
    inventoryCore[currentDisplayedCore].visible = true;
    inventoryGroup.push(inventoryCore[currentDisplayedCore]);
}

function spinDisplayedCore() {
    inventoryCore[currentDisplayedCore].rotation.y -= 0.005;
}

function spinCoreHolder() {
    inventoryGroup[0].rotation.y += 0.0005;
}

function swapCore(direction) {
    if ((currentDisplayedCore == 0 && direction == -1) || (currentDisplayedCore == inventoryCore.length - 1 && direction == 1)) {
        // display message ?
        return;
    }

    inventoryCore[currentDisplayedCore].visible = false;

    switch (direction) {
        case 1:
        console.log(direction);
        if (currentDisplayedCore < CORE_TYPE.CORE_TOTAL) {
            currentDisplayedCore++;
        }
        while (currentDisplayedCore < CORE_TYPE.CORE_TOTAL && collection[currentDisplayedCore] == 0 ) {
            console.log(currentDisplayedCore);
            currentDisplayedCore++;
        }
            break;
        case -1:
        console.log(direction);
        if (currentDisplayedCore < collection[currentDisplayedCore] == 0) {
            currentDisplayedCore--;
        }
        while (currentDisplayedCore > 0 && collection[currentDisplayedCore] == 0 ) {
            console.log(currentDisplayedCore);
            currentDisplayedCore--;
        }
            break;
    }

    inventoryCore[currentDisplayedCore].visible = true;
    displayGroup[1] = inventoryCore[currentDisplayedCore];
}

function addCoreHolder() {
    geometry = new THREE.DodecahedronGeometry(50, 0);
    material = new THREE.MeshBasicMaterial({
        wireframe: true,
        transparent: true,
        opacity: 0.15
    });
    let coreHolder = new THREE.Mesh(geometry, material);

    coreHolder.position.set(0, 0, 0);

    inventoryGroup[inventoryGroup.length] = coreHolder;

    scene.add(coreHolder);
}

function addLighting() {
    let ambientLight = new THREE.AmbientLight(0x777777); // soft white light
    scene.add(ambientLight);

    let coreLight = new THREE.PointLight(0x6495ed);
    coreLight.position.set(0, 0, 0);
    scene.add(coreLight);
}

function addCoreToScene() {
    // fetch cores to display from locale liste
    let i = 0;
    let bufMap;
    let bufNormalMap;
    let geometry = new THREE.SphereBufferGeometry(25, 32, 32);
    let material;
    let core;

    for (el of Object.keys(texture)) {
        material = new THREE.MeshPhongMaterial({
            normalMap: texture[el].normalMap,
            map: texture[el].image
        });

        core = new THREE.Mesh(geometry, material);

        inventoryCore.push(core);
        core.visible = false;
        core.position.set(0, 0, 0);
        scene.add(core);
    }
}
