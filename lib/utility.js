function toggleFade() {
    console.log("blackscreen toggled");
    $("#blackscreen").toggleClass("fade-in");
}

function toggleTryptic() {
    $("#tryptic-container").toggleClass("fade-in");
}

function cleanScene(keepHelpers) {

    for (el of Object.keys(scene.children)) {
        scene.children[el].visible = false;
    }

    if ((keepHelpers)) {
        let axes = new THREE.AxesHelper(1000);
        axes.position.set(0, 0, 0);
        scene.add(axes);
    }
}

function restoreScene() {
    for (el of Object.keys(scene.children)) {
        scene.children[el].visible = true;
    }

    if (currentState == GAME_STATE.INVENTORY) {
        for (el of Object.keys(inventoryGroup)) {
            inventoryGroup[el].visible = false;
        }
        lockCamOnChar();
    }
}

function initLS() {
    let ls = localStorage;
    let collectionBuffer = '';
    let collectionStr = '';
    let collection = [];
    let hasSeen = new Array(ATTR.ATTR_TOTAL).fill(0);

    ls.setItem("init", true);
    ls.setItem("collection", JSON.stringify(collection));
    ls.setItem("hasSeen", JSON.stringify(hasSeen));
}

function handleLocalSession() {
    let ls = localStorage;
    let rawCollection;
    let collectionBuffer;

    let rawHasSeen;
    let hasSeenBuffer;

    if (ls.getItem("init") == null) {
        initLS();
    } else {
        rawCollection = ls.getItem("collection");
        if ((rawCollection)) {
            collectionBuffer = JSON.parse(rawCollection);
            collection = collectionBuffer.map(n => parseInt(n));
            console.log(collection);
        }

        rawHasSeen = ls.getItem("hasSeen");
        if ((rawHasSeen)) {
            hasSeenBuffer = JSON.parse(rawHasSeen);
            hasSeen = hasSeenBuffer.map(n => parseInt(n));
            console.log(hasSeen);
        }

    }
}

function addToCollection(id) {
    let ls = localStorage;

    if (!(collection.includes(id))) {
        collection.push(id);
        ls.setItem("collection", JSON.stringify(collection));
        swal("Félicitations !", "Tu as découvert un noyau de type " + id + " !", "success");
    }
}

function checkDiscovered() {
    let i = 0;
    let restart = 1;
    let skip = new Array(ATTR.ATTR_TOTAL).fill(0);

    while (restart) {
        i = 0;
        restart = 0;
        for (el of Object.keys(planets.children[activePlanetIndex].attributes)) {
            if (planets.children[activePlanetIndex].attributes[el].status == 1 && !(skip[i])) {
                if (hasSeen[i] == 0) {
                    discovered(i);
                    restart = 1;
                    skip[i] = 1;
                    break;
                }
            }
            i++;
        }
    }
}

function discovered(id) {
    let ls = localStorage;

    let str;

    switch (id) {
        case ATTR.CORE:
            str = "une planète à noyau";
            break;
        case ATTR.ASTEROIDS:
            str = "une planète situé dans un champ d'astéroïdes"
            break;
        case ATTR.SHELL:
            str = "des astéroïdes atmosphériques"
            break;
        case ATTR.TAIL:
            str = "une comète fossilisée"
            break;
        case ATTR.MOVEMENT:
            str = "une planète déformé par une courbure de l'espace-temps"
            break;
        case ATTR.GRAVITY_STRONG:
            str = "une planète à la gravité renforcée"
            break;
        case ATTR.GRAVITY_WEAK:
            str = "une planète à la gravité affaiblie"
            break;
        case ATTR.GIANT:
            str = "une planète géante"
            break;
        case ATTR.TINY:
            str = "une planète naine"
            break;
        case ATTR.NAUTILUS:
            str = "les débris d'un nautilus fossilisé galactique"
            break;
    }

    if (hasSeen[id] == 0) {
        hasSeen[id] = 1;
        ls.setItem("hasSeen", JSON.stringify(hasSeen));
        swal("Félicitations !", "Vous avez découvert pour la première fois " + str + " !", "success");
    }
}

function resetLS() {
    let ls = localStorage;
    let hasSeen = new Array(ATTR.ATTR_TOTAL).fill(0);

    ls.setItem("init", false);
    ls.setItem("collection", "");
    ls.setItem("hasSeen", JSON.stringify(hasSeen));
}

function lockCamOnChar() {
    camera.position.set(char.position.x + 500, char.position.y + 500, char.position.z + 500);
    camera.lookAt(char.position);
}

function unlockCam() {
    //camera.lookAt(0, 0, 1);
    camera.position.set(0, 0, 0);
    camera.rotation.set(0,0,0);
}
