function generateWarpCylinder(rtop, rbot, h, isOpen) {
    let tunnelCylinders = [];
    let geometry = new THREE.CylinderGeometry(rtop, rbot, h, 32, 1, isOpen);

    cruisingWarpTexture = new THREE.TextureLoader().load("texture/water_tainted.jpg");
    cruisingWarpTexture.wrapT = THREE.RepeatWrapping;

    warpTunnelTexture = new THREE.TextureLoader().load("texture/snowflakes.jpg");
    warpTunnelTexture.wrapT = THREE.RepeatWrapping;

    cruisingWarpStarsTexture = new THREE.TextureLoader().load("texture/stars_more_dense.jpg");
    cruisingWarpStarsTexture.wrapT = THREE.RepeatWrapping;

    let material = new THREE.MeshLambertMaterial({
        color: 0xffffff,
        //map: warpTunnelTexture,
        alphaMap: warpTunnelTexture,
        side: THREE.DoubleSide,
        transparent: true,
        opacity: 0
    });
    let cylinder = new THREE.Mesh(geometry, material);

    cylinder.rotation.x = Math.PI / 2;

    //cylinder.position.set(camera.x, camera.y, camera.z+50);

    tunnelCylinders.push(cylinder);
    scene.add(cylinder);

    geometry = new THREE.CylinderGeometry(1.5, 1.5, 100, 32, 1, isOpen);

    material = new THREE.MeshLambertMaterial({
        //color: 0xffffff,
        //map: warpTunnelTexture,
        alphaMap: cruisingWarpTexture,
        side: THREE.DoubleSide,
        transparent: true,
        opacity: 0
    });

    cylinder = new THREE.Mesh(geometry, material);

    cylinder.visible = false;

    cylinder.rotation.x = Math.PI / 2;
    //cylinder.position.set(camera.x, camera.y, camera.z+50);

    tunnelCylinders.push(cylinder);
    scene.add(cylinder);

    geometry = new THREE.CylinderGeometry(1.3, 1.3, 50, 32, 1, isOpen);

    material = new THREE.MeshLambertMaterial({
        //color: 0xffffff,
        //map: warpTunnelTexture,
        alphaMap: cruisingWarpStarsTexture,
        side: THREE.DoubleSide,
        transparent: true,
        opacity: 0
    });

    cylinder = new THREE.Mesh(geometry, material);

    cylinder.visible = false;

    cylinder.rotation.x = Math.PI / 2;
    //cylinder.position.set(camera.x, camera.y, camera.z+50);

    tunnelCylinders.push(cylinder);
    scene.add(cylinder);

    return tunnelCylinders;
}

function animateTunnel() {
    warpTunnelTexture.offset.y += textureOffset;
    warpTunnelTexture.offset.y %= 1;

    //warpTunnelTexture.needsUpdate = true;

    cruisingWarpTexture.offset.y += textureOffset;
    cruisingWarpTexture.offset.y %= 1;

    //cruisingWarpTexture.needsUpdate = true;

    cruisingWarpStarsTexture.offset.y += textureOffset;
    cruisingWarpStarsTexture.offset.y %= 1;

    //cruisingWarpStarsTexture.needsUpdate = true;

    //let seconds = Date.now() / 1000;
    //let radius = 0.05;
    //let angle = Math.sin(0.75 * seconds * Math.PI) / 4;

    //angle = (seconds * Math.PI) / 4;
    //camera.position.x = Math.cos(angle - Math.PI / 2) * radius;
    //camera.position.y = Math.sin(angle - Math.PI / 2) * radius;
    //camera.rotation.z = angle;
}

function enterTunnel() {
    createTunnel();
}

function leaveWarp() {
    /*planets.children[activePlanetIndex].visible = true;
    initIsometricCamera();
    restoreScene();
    camera.position.set(500,500,500);
    lockCamOnChar();*/
    warpPace = WARP.ARRIVAL;
}

function enterWarp() {
    planets.children[activePlanetIndex].visible = false;
    warpPace = WARP.INIT;
}

function animateWarp() {
    switch (warpPace) {
        case WARP.INIT:
            console.log("warp init");
            /*if (warpTunnel[0].material.opacity < 0.40) {
                warpTunnel[0].material.opacity += 0.01;
                fovModifier -= fovFriction/8;
                camera.fov += fovModifier/8;
                camera.updateProjectionMatrix();
                console.log(camera.fov);

            } else */
            if (camera.fov > 4) {
                if (warpTunnel[0].material.opacity < 0.99) {
                    warpTunnel[0].material.opacity += 0.005;
                }
                /*
                                                if (camera.fov > 90) {
                                                    fovFriction *= 1.3;
                                                }
                */
                fovModifier -= fovFriction;
                camera.fov += fovModifier * 1.3;
                camera.updateProjectionMatrix();
                console.log(camera.fov);

            } else {
                warpTunnel[0].material.opacity = 0;
                warpPace = WARP.FASTER;
                fovModifier = 2;
                swapTunnel();
            }
            break;
        case WARP.FASTER:
            console.log("warp faster");
            //shakeCamera();
            if (!(fovModified)) {
                camera.fov = 70
                fovModified = 1;
            }
            if (camera.fov > 20) {
                if (camera.fov > 35) {

                    camera.fov -= fovModifier / 10;
                } else {
                    camera.fov -= fovModifier / 4;
                }
                camera.updateProjectionMatrix();

                if (warpTunnel[2].material.opacity < 0.99) {
                    warpTunnel[2].material.opacity += 0.005;
                }

                textureOffset = 0.01;
                if ((tunnelSwapped)) {
                    if (cruisingWarpStarsTexture.offset.y < 0.8) {
                        textureOffset += 0.0001;
                    }
                    if (warpTunnel[1].material.opacity < 0.8) {
                        warpTunnel[1].material.opacity += 0.02;
                    }
                }

            } else if (warpTunnelTexture.offset.y < 0.8) {
                if (warpTunnel[2].material.opacity < 0.5) {
                    warpTunnel[2].material.opacity += 0.005;
                }

                textureOffset = 0.01;
                if ((tunnelSwapped)) {
                    if (cruisingWarpStarsTexture.offset.y < 0.8) {
                        textureOffset += 0.0001;
                    }
                    if (warpTunnel[1].material.opacity < 0.8) {
                        warpTunnel[1].material.opacity += 0.02;
                    }
                }

            } else {
                fovModified = 0;
                warpPace = WARP.CRUISING;
                fovModifier = 2;
            }
            break;
        case WARP.CRUISING:
            //flickerLight();
            console.log("warp cruising");
            if (!(clock.running)) {
                clock.start();
            } else if (clock.getElapsedTime() >= warpDuration) {
                warpPace = WARP.SLOWER;
                clock.stop();
            } else {
                /*varianceCounter++;
                if (varianceCounter % 60 == 0) {
                    varianceModifier *= -1;
                }
*/
                /*variance = Math.sin((Date.now() / 100) * Math.PI);
                console.log(variance);
                //variance %= 1;*/

                /*camera.fov += varianceModifier;
                camera.updateProjectionMatrix();

                console.log(camera.fov);*/
            }
            break;
        case WARP.SLOWER:
            console.log("warp slower");
            if (warpTunnelTexture.offset.y > 0.2) {
                textureOffset -= 0.0001;
                if (camera.fov > 10) {
                    if (fovModifier > 0.005) {
                        fovModifier -= fovFriction;
                    }
                    camera.fov -= fovModifier;
                    camera.updateProjectionMatrix();
                } else {
                    fovFriction = 2.5;
                    fovModified = 1;
                }
                if ((fovModified) && camera.fov < 60) {
                    if (fovModifier > 0.005) {
                        fovModifier -= fovFriction;
                    }
                    camera.fov += fovModifier;
                    camera.updateProjectionMatrix();
                    fovModified = 1;
                }

            } else {
                if (warpTunnel[0].material.opacity > 0) {
                    warpTunnel[0].material.opacity -= 0.02;
                }
                if (warpTunnel[1].material.opacity > 0) {
                    warpTunnel[1].material.opacity -= 0.02;
                }

                if (warpTunnel[2].material.opacity > 0) {
                    warpTunnel[2].material.opacity -= 0.02;
                }
                if (warpTunnel[0].material.opacity <= 0 && warpTunnel[1].material.opacity <= 0 && warpTunnel[2].material.opacity <= 0) {
                    leaveWarp();
                }
            }
            break;

        case WARP.ARRIVAL:
            console.log("warp arrival");
            warpPace = WARP.STANDBY;
            currentState = GAME_STATE.LANDING;
            break;
    }

    animateTunnel();
}

function swapTunnel() {
    if (!(tunnelSwapped)) {
        tunnelSwapped = 1;
        warpTunnel[0].visible = false;
        warpTunnel[1].visible = true;
        warpTunnel[2].visible = true;
    }
}

function initWarp() {
    warpEngagedSound.play();
    initWarpSound.play();
    //cleanScene();

    warpPace = WARP.INIT;

    tunnelSwapped = 0;

    // 3 to 8 seconds
    warpDuration = Math.ceil(Math.random() * 6) + 2;

    createTunnel();
}

function initWarpCamera() {
    camera = new THREE.PerspectiveCamera(40, aspectRatio, 0.1, 10000);
    camera.lookAt(scene.position);
}

function flickerLight() {
    if (isLightActive) {
        for (var i = 0; i < tunnelLight; i++) {
            tunnelLight[i].visible = false;
        }
        isLightActive = false;
    }

    probRng = Math.ceil(Math.random() * 60);

    if (probRng == 1) {
        rng = Math.ceil(Math.random() * 3) - 1;
        tunnelLight[rng].visible = true;
        isLightActive = true;
    }

}

function shakeCamera() {
    rngx = Math.random();
    rngy = Math.random();
    rngz = Math.random();

    probRng = parseInt(Math.random() * 3);

    console.log(probRng);
    if (probRng == 1) {
        switch (previousShakeCase) {
            case 0:
                camera.position.x -= previousShakeValuex;
            case 1:
                camera.position.y -= previousShakeValuey;
            case 2:
                camera.position.z -= previousShakeValuez;
        }

        switch (probRng) {
            case 0:
                camera.position.x += rngx;
            case 1:
                camera.position.y += rngy;
            case 2:
                camera.position.z += rngz;
        }

        previousShakeValuex = rngx;
        previousShakeValuey = rngy;
        previousShakeValuez = rngz;
        previousShakeCase = probRng
    }

}

function createTunnel() {
    //cleanScene(true);

    planets.children[activePlanetIndex].visible = false;

    tunnelLight[0] = new THREE.DirectionalLight(0xffffff, 1.5);
    tunnelLight[0].position.set(1, 1, 0).normalize();
    tunnelLight[0].visible = false;
    scene.add(tunnelLight);

    tunnelLight[1] = new THREE.DirectionalLight(0x444444, 1.5);
    tunnelLight[1].position.set(-1, 1, 0).normalize();
    tunnelLight[1].visible = false;
    scene.add(tunnelLight[1]);

    tunnelLight[2] = new THREE.PointLight(0xcccccc, 15, 25);
    tunnelLight[2].position.set(0, -3, 0);
    tunnelLight[2].visible = false;
    scene.add(tunnelLight[2]);

    currentState = GAME_STATE.WARP;
    warpTunnel = generateWarpCylinder(5, 5, 100, true);
    //unlockCam();
    scene.fog = new THREE.FogExp2(0x000000, 0.1);
}
