function createCube(x, y, z, texture) {
    let mesh;

    let geometry = new THREE.BoxGeometry(x, y, z);

    let material = new THREE.MeshPhongMaterial({
        map: texture
    });

    mesh = new THREE.Mesh(geometry, material);

    return mesh;
}

function createCone(radius, h, radSegs, texture) {
    let mesh;

    let geometry = new THREE.ConeGeometry(radius, h, radSegs);

    let material = new THREE.MeshPhongMaterial({
        map: texture
    });

    mesh = new THREE.Mesh(geometry, material);

    return mesh;
}

function createPlane(x, y, texture) {
    let mesh;

    let geometry = new THREE.PlaneGeometry(x, y, 32);

    let material = new THREE.MeshPhongMaterial({
        map: texture,
        transparent: true,
        side: THREE.DoubleSide
    });

    mesh = new THREE.Mesh(geometry, material);

    return mesh;
}

function setXYZ(x, y, z, el) {
    el.position.x = x;
    el.position.y = y;
    el.position.z = z;
}

function draw2DGround(map) {
    let cubeW = 20;
    let cubeH = 20;
    let cubeZ = 20;

    let tempMesh;

    for (let i = 0; i < map.length; i++) {
        for (let j = 0; j < map[0].length; j++) {
            tempMesh = createCube(cubeW, cubeH, cubeZ, groundTexture[map[i][j]]);
            tempMesh.position.x = i * cubeW;
            tempMesh.position.z = j * cubeW;
            scene.add(tempMesh)
        }
    }
}

function drawCircle(xa, ya, z, cubeSize, radius) {
    let x;
    let y;
    let dp;
    let tempMesh = [];
    let group = new THREE.Group();

    x = 0;
    y = radius;
    dp = 5 - 4 * radius;

    while (y >= x) {
        if (dp <= 0) {
            dp = dp + 8 * x + 12;
        } else {
            dp = dp + 8 * (x - y) + 20;
            y = y - cubeSize;
        }
        x += cubeSize;

        tempMesh[0] = createCube(cubeSize, cubeSize, cubeSize, groundTexture[0]);
        tempMesh[0].position.set(x + xa, y + ya, z);

        tempMesh[1] = createCube(cubeSize, cubeSize, cubeSize, groundTexture[0]);
        tempMesh[1].position.set(y + xa, x + ya, z);

        tempMesh[2] = createCube(cubeSize, cubeSize, cubeSize, groundTexture[0]);
        tempMesh[2].position.set((-x) + xa, y + ya, z);

        tempMesh[3] = createCube(cubeSize, cubeSize, cubeSize, groundTexture[0]);
        tempMesh[3].position.set((-y) + xa, x + ya, z);

        tempMesh[4] = createCube(cubeSize, cubeSize, cubeSize, groundTexture[0]);
        tempMesh[4].position.set(x + xa, (-y) + ya, z);

        tempMesh[5] = createCube(cubeSize, cubeSize, cubeSize, groundTexture[0]);
        tempMesh[5].position.set(y + xa, (-x) + ya, z);

        tempMesh[6] = createCube(cubeSize, cubeSize, cubeSize, groundTexture[0]);
        tempMesh[6].position.set((-x) + xa, (-y) + ya, z);

        tempMesh[7] = createCube(cubeSize, cubeSize, cubeSize, groundTexture[0]);
        tempMesh[7].position.set((-y) + xa, (-x) + ya, z);

        for (cube of tempMesh) {
            group.add(cube);
            //scene.add(cube);
        }
    }

    return group;
}

function drawPlanet(r, lats, longs, cubeSize, mode) {

}

function createPlanet(r, lats, longs, cubeSize, attr) {
    let mesh;
    let i = 0.0;
    let j = 0.0;
    let k = 0;
    let m = 0;
    let lat0 = 0.0;
    let z0 = 0.0;
    let zr0 = 0.0;
    let lat1 = 0.0;
    let zr1 = 0.0;
    let z1 = 0.0;
    let lng = 0.0;
    let x = 0.0;
    let y = 0.0;
    let crtZ = 0;

    let cnt = 0;
    let quit = false;

    let rng = 0;

    let heightQuantum;

    let planet = new THREE.Group();

    for (el of Object.keys(attr)) {
        if (attr[el].status == 1) {
            switch (cnt) {
                case ATTR.TAIL:
                    heightQuantum = 5;

                    for (i = 0.0; i <= lats; i++) {
                        lat0 = Math.PI * (-0.5 + (i - 1.0) / lats);
                        z0 = Math.sin(lat0) * r;
                        zr0 = Math.cos(lat0) * r;

                        lat1 = Math.PI * (-0.5 + i / lats);
                        z1 = Math.sin(lat1) * r;
                        zr1 = Math.cos(lat1) * r;

                        for (j = 0.0; j <= longs; j++) {
                            lng = 2.0 * Math.PI * (j - 1.0) / longs;
                            x = Math.cos(lng);
                            y = Math.sin(lng);

                            // Classic ball planet
                            mesh = createCube(cubeSize, cubeSize, cubeSize, groundTexture[0]);
                            mesh.position.set(x * zr0, y * zr0, z0, 0);
                            planet.add(mesh);

                            // Bottom drifting planet
                            for (k = 0, crtZ = 0; k < Math.ceil(Math.random() * 100) % heightQuantum; k++) {
                                if ((x * zr0) - (crtZ * cubeSize) != x * zr0 && y * zr0 != (y * zr0) - (crtZ * cubeSize) * 2) {
                                    mesh = createCube(cubeSize, cubeSize, cubeSize, groundTexture[0]);
                                    mesh.position.set(x * zr0, (y * zr0) - (crtZ * cubeSize) * 2, z0, 0);
                                    planet.add(mesh);
                                }

                                crtZ++;
                            }
                        }
                    }
                    quit = true;
                    break;
                case ATTR.NAUTILUS:
                    console.log("has nautilus !");
                    heightQuantum = 10;
                    for (i = 0.0; i <= lats; i++) {
                        lat0 = Math.PI * (-0.5 + (i - 1.0) / lats);
                        z0 = Math.sin(lat0) * r;
                        zr0 = Math.cos(lat0) * r;

                        lat1 = Math.PI * (-0.5 + i / lats);
                        z1 = Math.sin(lat1) * r;
                        zr1 = Math.cos(lat1) * r;

                        for (j = 0.0; j <= longs; j++) {
                            lng = 2.0 * Math.PI * (j - 1.0) / longs;
                            x = Math.cos(lng);
                            y = Math.sin(lng);

                            // Bottom drifting planet
                            for (k = 0, crtZ = 0; k < Math.ceil(Math.random() * 100) % heightQuantum; k++) {
                                if ((x * zr0) - (crtZ * cubeSize) != x * zr0 && y * zr0 != (y * zr0) - (crtZ * cubeSize) * 2) {

                                    rng = Math.ceil(Math.random() * 3);

                                    if (rng == 1) {
                                        rng = Math.ceil(Math.random() * heightQuantum);
                                        rng += heightQuantum;
                                        zr0 += rng;

                                        mesh = createCube(cubeSize, cubeSize, cubeSize, groundTexture[0]);
                                        mesh.position.set(x * (zr0 + rng), y * (zr0 + rng), z0 + rng, 0);
                                        planet.add(mesh);
                                    }

                                }

                                crtZ++;
                            }
                        }
                    }

                    quit = true;
                    break;
                case ATTR.GIANT:
                    lats *= 1.4;
                    longs *= 1.4;
                    r *= 2;

                    for (i = 0.0; i <= lats; i++) {
                        lat0 = Math.PI * (-0.5 + (i - 1.0) / lats);
                        z0 = Math.sin(lat0) * r;
                        zr0 = Math.cos(lat0) * r;

                        lat1 = Math.PI * (-0.5 + i / lats);
                        z1 = Math.sin(lat1) * r;
                        zr1 = Math.cos(lat1) * r;

                        for (j = 0.0; j <= longs; j++) {
                            lng = 2.0 * Math.PI * (j - 1.0) / longs;
                            x = Math.cos(lng);
                            y = Math.sin(lng);

                            // Classic ball planet
                            mesh = createCube(cubeSize, cubeSize, cubeSize, groundTexture[0]);
                            mesh.position.set(x * zr0, y * zr0, z0, 0);
                            planet.add(mesh);
                        }
                    }
                    quit = true;
                    break;
                case ATTR.TINY:
                    lats *= 0.7;
                    longs *= 0.7;
                    r *= 0.5;

                    for (i = 0.0; i <= lats; i++) {
                        lat0 = Math.PI * (-0.5 + (i - 1.0) / lats);
                        z0 = Math.sin(lat0) * r;
                        zr0 = Math.cos(lat0) * r;

                        lat1 = Math.PI * (-0.5 + i / lats);
                        z1 = Math.sin(lat1) * r;
                        zr1 = Math.cos(lat1) * r;

                        for (j = 0.0; j <= longs; j++) {
                            lng = 2.0 * Math.PI * (j - 1.0) / longs;
                            x = Math.cos(lng);
                            y = Math.sin(lng);

                            // Classic ball planet
                            mesh = createCube(cubeSize, cubeSize, cubeSize, groundTexture[0]);
                            mesh.position.set(x * zr0, y * zr0, z0, 0);
                            planet.add(mesh);
                        }
                    }
                    quit = true;
                    break;

            }
        }

        if (quit == true) {
            break;
        }

        cnt++;
    }

    // If no special planet, go for the standard one
    if (quit == false) {
        for (i = 0.0; i <= lats; i++) {
            lat0 = Math.PI * (-0.5 + (i - 1.0) / lats);
            z0 = Math.sin(lat0) * r;
            zr0 = Math.cos(lat0) * r;

            lat1 = Math.PI * (-0.5 + i / lats);
            z1 = Math.sin(lat1) * r;
            zr1 = Math.cos(lat1) * r;

            for (j = 0.0; j <= longs; j++) {
                lng = 2.0 * Math.PI * (j - 1.0) / longs;
                x = Math.cos(lng);
                y = Math.sin(lng);

                // Classic ball planet
                mesh = createCube(cubeSize, cubeSize, cubeSize, groundTexture[0]);
                mesh.position.set(x * zr0, y * zr0, z0, 0);
                planet.add(mesh);
            }
        }
    }

    planet.radius = r;
    planet.lats = lats;
    planet.longs = longs;

    planet.animationSpeed = 0.02;
    planet.moveState = [0, 0, 0, 0];
    planet.gravity = 2;

    cnt = 0;

    for (el of Object.keys(attr)) {
        if ((attr[el].status)) {
            switch (cnt) {
                case ATTR.CORE:
                    randomizeCore(planet);
                    break;
                case ATTR.ASTEROIDS:
                    asteroids = createAsteroids();
                    break;
                case ATTR.SHELL:
                    shellGroup = generateShell(planet);
                    break;
                case ATTR.MOVEMENT:
                    doShakePlanet = 1;
                    break;
                case ATTR.GRAVITY_STRONG:
                    planet.gravity *= 2;
                    break;
                case ATTR.GRAVITY_WEAK:
                    planet.gravity /= 2;
                    break;
            }

        }
        cnt++;
    }

    planet.attributes = JSON.parse(JSON.stringify(attr));

    return planet;
}

function randomizeAttribute() {
    let i = 0;
    let cnt = 0;
    let rng;

    let pool;

    let roundKey = 0;

    let attrClone = JSON.parse(JSON.stringify(planets.attributes));

    for (attr of Object.keys(planets.attributes)) {
        let prob = planets.attributes[attr].prob;

        pool = new Array(prob).fill(-1);

        pool[0] = 1;

        rng = (Math.floor(Math.random() * prob));

        console.log(attr);
        console.log(rng);
        console.log(pool[rng]);
        if (pool[rng] != -1) {
            attrClone[attr].status = 1;
            console.log("Bingo", cnt);
        }
        cnt++;
    }

    return attrClone;
}

function generateAttribute(planet, attr) {
    switch (attr) {
        case ATTR.CORE:
            randomizeCore(planet);
            break;
        case ATTR.ASTEROIDS:
            asteroids = createAsteroids();
            break;
        case ATTR.SHELL:
            shellGroup = generateShell(planet);
            break;
        case ATTR.TAIL:
            createPlanet(attr);
            break;
        case ATTR.MOVEMENT:
            doShakePlanet = 1;
            break;
        case ATTR.GRAVITY_STRONG:
            char.gravity *= 2;
            break;
        case ATTR.GRAVITY_WEAK:
            char.gravity /= 2;
            break;
        case ATTR.GIANT:
            createPlanet(attr);
            break;
        case ATTR.TINY:

            createPlanet(attr);
        case ATTR.NAUTILUS:
            createPlanet(attr);
            break;

    }
}

function shakePlanet() {

}

function randomizeCore(planet) {
    let rng = Math.ceil((Math.random() * CORE_TYPE.CORE_TOTAL));

    generateCore(rng);
}

function addNautilus() {


}

function createShell(r, lats, longs, cubeSize) {
    let mesh;
    let i = 0.0;
    let j = 0.0;
    let k = 0;
    let lat0 = 0.0;
    let z0 = 0.0;
    let zr0 = 0.0;
    let lat1 = 0.0;
    let zr1 = 0.0;
    let z1 = 0.0;
    let lng = 0.0;
    let x = 0.0;
    let y = 0.0;
    let crtZ = 0;
    let rng = 0;

    let shellPart = [];

    shellPart[0] = new THREE.Group();
    shellPart[1] = new THREE.Group();
    shellPart[2] = new THREE.Group();

    // ADD FLOAT
    for (i = 0.0; i <= lats; i++) {
        lat0 = Math.PI * (-0.5 + (i - 1.0) / lats);
        z0 = Math.sin(lat0) * r;
        zr0 = Math.cos(lat0) * r;

        lat1 = Math.PI * (-0.5 + i / lats);
        z1 = Math.sin(lat1) * r;
        zr1 = Math.cos(lat1) * r;

        for (j = 0.0; j <= longs; j++) {
            lng = 2.0 * Math.PI * (j - 1.0) / longs;
            x = Math.cos(lng);
            y = Math.sin(lng);

            rng = Math.ceil((Math.random() * 4));

            if (rng == 1) {
                mesh = createCube(cubeSize, cubeSize, cubeSize, groundTexture[0]);
                mesh.position.set(x * (zr0 + r) , y * (zr0 + r), z0, 0);

                // 0 - 2
                rng = Math.ceil((Math.random() * 100) % 3)-1;
                shellPart[rng].add(mesh);
            }
        }
    }

    return shellPart;
}

function generateShell(planet) {
    let i = 0;
    let shellBuf = createShell(planet.radius * 2, planet.lats / 1.5, planet.longs / 1.5, 10.0);

    //for (i = 0; i < 2; i++) {
        shellBuf[i].position.set(0, 0, 0);
        scene.add(shellBuf[i]);
    //}

    return shellBuf;
}

function generateCore(type) {
    currentCore = createCore(type);

    currentCore.position.set(0, 0, 0);
    currentCore.typeId = type;
    scene.add(currentCore);
}

function createAsteroids() {
    let i = 0;
    let asteroids = [];
    let maxWidth = 50;
    let maxHeight = 10;
    let maxDepth = 10;

    for (i = 0; i < 7; i++) {
        asteroids.push(createRock(5 + Math.random() * 50, 200, maxWidth, 300, 400));
    }
    for (i = 0; i < 30; i++) {
        asteroids.push(createRock(5 + Math.random() * 10, 500, maxWidth, 200, 600));
    }
    for (i = 0; i < 50; i++) {
        asteroids.push(createRock(2 + Math.random() * 5, 1000, maxWidth, 150, 800));
    }
    return asteroids;
}


function createRock(size, spreadX, maxWidth, maxHeight, maxDepth) {
    let planet = planets.children[activePlanetIndex];
    let safeSpace = 335;
    let color = '#111111';
    let geometry = new THREE.DodecahedronGeometry(size, 1);
    geometry.vertices.forEach(function(v) {
        v.x += (0 - Math.random() * (size / 4));
        v.y += (0 - Math.random() * (size / 4));
        v.z += (0 - Math.random() * (size / 4));
    })

    color = colorLuminance(color, 2 + Math.random() * 10);

    let texture = new THREE.MeshStandardMaterial({
        color: color,
        flatShading: true,
        roughness: 0.8,
        metalness: 1
    });

    let cube = new THREE.Mesh(geometry, texture);
    cube.castShadow = true;
    cube.receiveShadow = true;
    cube.scale.set(1 + Math.random() * 0.4, 1 + Math.random() * 0.8, 1 + Math.random() * 0.4);
    //cube.rotation.y = Math.PI/4;
    //cube.rotation.x = Math.PI/4;
    let x = spreadX / 2 - Math.random() * spreadX + safeSpace;
    let centeredness = 1 - (Math.abs(x) / (maxWidth / 2));
    let y = (maxHeight / 2 - Math.random() * maxHeight) * centeredness + safeSpace;
    let z = (maxDepth / 2 - Math.random() * maxDepth) * centeredness + safeSpace;
    cube.position.set(x, y, z)
    cube.r = {};
    cube.r.x = Math.random() * 0.005;
    cube.r.y = Math.random() * 0.005;
    cube.r.z = Math.random() * 0.005;

    scene.add(cube);
    return cube;
}

function colorLuminance(hex, lum) {
    // validate hex string
    hex = String(hex).replace(/[^0-9a-f]/gi, '');
    if (hex.length < 6) {
        hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
    }
    lum = lum || 0;

    // convert to decimal and change luminosity
    let rgb = "#",
        c, i;
    for (i = 0; i < 3; i++) {
        c = parseInt(hex.substr(i * 2, 2), 16);
        c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
        rgb += ("00" + c).substr(c.length);
    }

    return rgb;
}

function updatePointCloud() {
    pointCloud.rotation.x -= 0.0001;
    //pointCloud.rotation.y -= 0.001;
    pointCloud.rotation.z -= 0.0001;

    if ((planets.children[activePlanetIndex].attributes.hasAsteroids.status)) {
        asteroids.forEach((obj) => {
            obj.rotation.x -= obj.r.x;
            obj.rotation.y -= obj.r.y;
            obj.rotation.z -= obj.r.z;
        });
    }
}

function generatePointCloud() {
    let material = new THREE.PointsMaterial({
        color: 0x555555
    });

    let geometry = new THREE.Geometry();
    let x, y, z;
    for (let i = 0; i < 2000; i++) {
        x = (Math.random() * w * 2) - w;
        y = (Math.random() * h * 2) - h;
        z = (Math.random() * 3000) - 1500;

        geometry.vertices.push(new THREE.Vector3(x, y, z));
    };

    pointCloud = new THREE.Points(geometry, material);
    scene.add(pointCloud);
}

function createCore(type) {
    let bufMap;
    let bufNormalMap;
    let geometry = new THREE.SphereBufferGeometry(40, 32, 32);
    let material;

    console.log("core type ", type);

    switch (type) {
        case CORE_TYPE.PLASTUER:
            bufNormalMap = texture.plastuer.normalMap;
            bufMap = texture.plastuer.image;
            break;
        case CORE_TYPE.MARBLE:
            bufNormalMap = texture.marble.normalMap;
            bufMap = texture.marble.image;
            break;
        case CORE_TYPE.STONE:
            bufNormalMap = texture.stone.normalMap;
            bufMap = texture.stone.image;
            break;
        case CORE_TYPE.ROUGH:
            bufNormalMap = texture.rough.normalMap;
            bufMap = texture.rough.image;
            break;
        case CORE_TYPE.ASPHALT:
            bufNormalMap = texture.asphalt.normalMap;
            bufMap = texture.asphalt.image;
            break;
        case CORE_TYPE.ABST_BUBBLY:
            bufNormalMap = texture.abstractBubbly.normalMap;
            bufMap = texture.abstractBubbly.image;
            break;
        case CORE_TYPE.ABST_SQUARE:
            bufNormalMap = texture.abstractSquare.normalMap;
            bufMap = texture.abstractSquare.image;
            break;
        case CORE_TYPE.METAL_PLATE:
            bufNormalMap = texture.metalPlate.normalMap;
            bufMap = texture.metalPlate.image;
            break;
        case CORE_TYPE.METAL_PLATE_REGULAR:
            bufNormalMap = texture.metalPlateRegular.normalMap;
            bufMap = texture.metalPlateRegular.image;
            break;
        case CORE_TYPE.ORANGE:
            bufNormalMap = texture.orange.normalMap;
            bufMap = texture.orange.image;
            break;
        case CORE_TYPE.STONE_FLOOR:
            bufNormalMap = texture.stoneFloor.normalMap;
            bufMap = texture.stoneFloor.image;
            break;
    }


    material = new THREE.MeshPhongMaterial({
        normalMap: bufNormalMap,
        map: bufMap
    });


    let core = new THREE.Mesh(geometry, material);

    return core;
}

function draw3DGround(map) {
    let i = 0;
    let j = 0;
    let k = 0;
    let cubeW = 20;
    let cubeH = 20;
    let cubeZ = 20;

    let mapX = 30;
    let mapY = 30;
    let mapZ = 3;

    let tempMesh;

    let simplex = new SimplexNoise(Math.random);

    let perlinMap = [];

    let max;

    for (i = 0; i < mapX; i++) {
        perlinMap[i] = [];
        for (j = 0; j < mapY; j++) {
            perlinMap[i][j] = [];
            max = ((simplex.p[Math.ceil(Math.random() * 100)] % 5) + 1) + 1;
            console.log(max);
            for (k = 0; k < max /*Math.ceil((simplex.noise2D(j,j)*1000)%20)*/ ; k++) {
                //perlinMap[i][j][k] = Math.ceil((simplex.noise2D(j,k)*1000)%20);
                perlinMap[i][j][k] = 0;
            }
        }
    }


    for (i = 0; i < perlinMap.length; i++) {
        for (j = 0; j < perlinMap[i].length; j++) {
            for (k = 0; k < perlinMap[i][j].length; k++) {
                tempMesh = createCube(cubeW, cubeH, cubeZ, groundTexture[perlinMap[i][j][k]]);
                tempMesh.position.x = i * cubeW;
                tempMesh.position.y = k * cubeH;
                tempMesh.position.z = j * cubeW;
                removableItems.push(tempMesh);
                scene.add(tempMesh);
            }
        }
    }

}
