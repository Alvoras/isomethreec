function displayMinMap()
{
    camera.position.x = 10000;
    camera.position.y = 0;
    camera.position.z = 500;
    camera.rotation.x = 0;
    camera.rotation.y = 0;
    camera.rotation.z = 0;

    planets.children[activePlanetIndex].visible = false;
}

function interfaceMinMap()
{
    for(i = 0; i<3; i++)
    {
        geometry = new THREE.CircleGeometry(50, 50);
        material = new THREE.MeshPhongMaterial( { color: 0xffff00 } );
        circlesMinMap.push(new THREE.Mesh( geometry, material ));
        circlesMinMap[i].position.set(9500 + (i * 500), 0, 0);
        scene.add( circlesMinMap[i] );
    }
}
